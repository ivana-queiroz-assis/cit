package br.com.ivana.cit.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

import br.com.ivana.cit.data.util.BebidaEnum;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@JsonTypeInfo(
		  use = JsonTypeInfo.Id.NAME, 
		  include = JsonTypeInfo.As.PROPERTY, 
		  property = "type",
		  visible = true)
		@JsonSubTypes({ 
		  @Type(value = BebidaAlcoolica.class, name = "ALCOOLICA"), 
		  @Type(value = BebidaNaoAlcoolica.class, name = "NAO_ALCOOLICA") 
		})
public class Bebida implements Serializable{

	 @JsonCreator
	  public Bebida(@JsonProperty("nome") final String nome, @JsonProperty("id") final String id, @JsonProperty("volume") int volume) {
	    super();
	    this.setNome(nome);
	    this.setId(Integer.parseInt(id));	
	    this.volume =volume;
	  }

	/**
	 * 
	 */
	private static final long serialVersionUID = -2310246821350526331L;

	public Bebida() {
		super();		
	}

	@Id	
	private int id;
	
	@ManyToOne
	@JsonBackReference
	private Secao secao;
	
	@Column
	private Integer volume;
	
	@Column
	private String nome;	

	@Enumerated(EnumType.STRING)
	private BebidaEnum type;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public Bebida(int id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
		
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Secao getSecao() {
		return secao;
	}

	public void setSecao(Secao secao) {
		this.secao = secao;
	}

	
	public BebidaEnum getType() {
		return type;
	}

	public void setType(BebidaEnum type) {
		this.type = type;
	}

	public Integer getVolume() {
		return volume;
	}

	public void diminuiVolume(int volume) {
		this.volume-=volume;
	}
	
	public void aumentaVolume() {
		this.volume+=volume;
	}

	

	

		
}
