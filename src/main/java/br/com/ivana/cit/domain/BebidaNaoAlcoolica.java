package br.com.ivana.cit.domain;

import java.io.Serializable;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Value;
@Entity
@Value
public class BebidaNaoAlcoolica extends Bebida implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9104451349643308515L;

	 @JsonCreator
	  public BebidaNaoAlcoolica(@JsonProperty("nome") final String nome, @JsonProperty("id") final String id, @JsonProperty("volume") int volume) {
		 super(nome,id,volume);
	  }
	 public BebidaNaoAlcoolica() {
			
		}
}
