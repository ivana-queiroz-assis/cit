package br.com.ivana.cit.domain;

import java.io.Serializable;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Value;
@Entity
@Value
public class BebidaAlcoolica extends Bebida implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = -261549448829863368L;
	@JsonCreator
	public BebidaAlcoolica(@JsonProperty("nome") final String nome, @JsonProperty("id") final String id, @JsonProperty("volume") int volume) {
	    super(nome,id,volume);
	 }
	
	public BebidaAlcoolica() {
		
	}
}
