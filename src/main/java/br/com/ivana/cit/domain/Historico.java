package br.com.ivana.cit.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.ivana.cit.data.util.FluxoEnum;
import br.com.ivana.cit.data.util.ResponsavelEnum;

@Entity
public class Historico implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1795084700690254861L;

	public Historico() {
		super();
	}

	@Id
	@GeneratedValue
	private int id;
	
	private int qntidade;
	
	@Enumerated(EnumType.STRING)
	private FluxoEnum fluxo;
	
	@ManyToOne
	private Secao secao;
	
	@ManyToOne
	private Bebida bebida;
	
	@Enumerated(EnumType.STRING)
	private ResponsavelEnum responsavel;
	
	public Historico(int qntidade, FluxoEnum fluxo,  Secao secao, Bebida bebida, Date dateTransacao,
			ResponsavelEnum responsavel) {
		super();
		this.qntidade = qntidade;
		this.fluxo = fluxo;		
		this.secao = secao;
		this.bebida = bebida;
		this.dateTransacao = dateTransacao;
		this.responsavel = responsavel;
	}

	public Historico(int id, int qntidade, FluxoEnum fluxo, Secao secao, Bebida bebida, ResponsavelEnum responsavel,
			Date dateTransacao) {
		super();
		this.id = id;
		this.qntidade = qntidade;
		this.fluxo = fluxo;
		this.secao = secao;
		this.bebida = bebida;
		this.responsavel = responsavel;
		this.dateTransacao = dateTransacao;
	}

	@Temporal(TemporalType.TIME)
	private Date dateTransacao;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQntidade() {
		return qntidade;
	}

	public void setQntidade(int qntidade) {
		this.qntidade = qntidade;
	}

	public FluxoEnum getFluxo() {
		return fluxo;
	}

	public void setFluxo(FluxoEnum fluxo) {
		this.fluxo = fluxo;
	}
	

	public Secao getSecao() {
		return secao;
	}

	public void setSecao(Secao secao) {
		this.secao = secao;
	}

	public Bebida getBebida() {
		return bebida;
	}

	public void setBebida(Bebida bebida) {
		this.bebida = bebida;
	}

	public Date getDateTransacao() {
		return dateTransacao;
	}

	public void setDateTransacao(Date dateTransacao) {
		this.dateTransacao = dateTransacao;
	}

	public ResponsavelEnum getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(ResponsavelEnum responsavel) {
		this.responsavel = responsavel;
	}

	
	
}
