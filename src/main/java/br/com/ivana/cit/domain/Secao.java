package br.com.ivana.cit.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.ivana.cit.data.util.BebidaEnum;

@Entity
@JsonPropertyOrder({ "id", "nome", "bebidas" })
public class Secao implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2850799967393245941L;

	@Id
	private int id;

	@Column
	private String nome;

	@ManyToOne
	@JoinColumn(name = "estoque_id")
	@JsonBackReference
	private Estoque estoque;

	@OneToMany(mappedBy = "secao", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonManagedReference
	private List<Bebida> bebidas;

	@Column
	@JsonIgnore
	private int volumeTotal;

	@Column
	@JsonIgnore
	private int volumeArmazenado;

	@Enumerated(EnumType.STRING)
	private BebidaEnum bebidaType;

	@JsonIgnore
	@DateTimeFormat(pattern = "DD/MM/YYYY")
	private Date ultimaAlteracaoTipo;

	public Secao(BebidaEnum bebidaType) {
		super();
		this.bebidaType = bebidaType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Estoque getEstoque() {
		return estoque;
	}

	public void setEstoque(Estoque estoque) {
		this.estoque = estoque;
	}

	public Secao(int id, String nome) {
		super();
		this.id = id;
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Bebida> getBebidas() {
		return bebidas;
	}

	public void setBebidas(List<Bebida> bebidas) {
		this.bebidas = bebidas;
	}

	public Secao() {
		super();
	}

	public int getVolumeTotal() {
		return this.volumeTotal;
	}

	public Secao(int id, String nome, Estoque estoque) {
		super();
		this.id = id;
		this.nome = nome;
		this.estoque = estoque;
	}

	public Secao(int id, String nome, Estoque estoque, BebidaEnum bebidaType, Date dateLastChangeType) {
		super();
		this.id = id;
		this.nome = nome;
		this.estoque = estoque;
		this.bebidaType = bebidaType;
		if (bebidaType.equals(BebidaEnum.NAO_ALCOOLICA)) {
			this.volumeTotal = 400;
		} else if (bebidaType.equals(BebidaEnum.ALCOOLICA)) {
			this.volumeTotal = 500;
		} else {
			this.volumeTotal = 0;
		}
		this.ultimaAlteracaoTipo=dateLastChangeType;
	}

	public int getVolumeArmazenado() {
		return volumeArmazenado;
	}

	public void aumentaVolumeArmazenado(int volume) {
		this.volumeArmazenado += volume;
	}

	public void diminuiVolumeArmazenado(int volume) {
		this.volumeArmazenado -= volume;
	}

	public BebidaEnum getBebidaType() {
		return bebidaType;
	}

	public boolean alteraTipoBebidaAndVolume(BebidaEnum bebidaType) {
		if (podeTrocarTipo(bebidaType) ) {
			this.bebidaType = bebidaType;
			if (bebidaType.equals(BebidaEnum.NAO_ALCOOLICA)) {
				this.volumeTotal = 400;
			} else if (bebidaType.equals(BebidaEnum.ALCOOLICA)) {
				this.volumeTotal = 500;
			} else {
				this.volumeTotal = 0;
			}
			return true;
		} else {
			return false;
		}
	}

	public boolean podeTrocarTipo(BebidaEnum bebidaType) {
		return !mesmoDiaUltimaAlteracao() || isSemTipo(bebidaType);
	}

	private boolean isSemTipo(BebidaEnum bebidaType) {
		return bebidaType.equals(BebidaEnum.SEM_TIPO);
	}
	
	@SuppressWarnings("deprecation")
	private boolean mesmoDiaUltimaAlteracao() {
		Date atual = new Date();
		return atual.getDay()==this.ultimaAlteracaoTipo.getDay();
	}
	
	public boolean tipoBebidaIsTheSame(BebidaEnum bebida) {
		return this.bebidaType.equals(bebida) ? true : false;
	}

	public boolean estaSemBebida() {
		return this.bebidaType.equals(BebidaEnum.SEM_TIPO) ? true : false;
	}

	public Date getUltimaAlteracaoTipo() {
		return ultimaAlteracaoTipo;
	}

	public void setUltimaAlteracaoTipo(Date ultimaAlteracaoTipo) {
		this.ultimaAlteracaoTipo = ultimaAlteracaoTipo;
	}
}
