package br.com.ivana.cit.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Entity
//@Scope(value="singleton")
@JsonPropertyOrder({"id","secao"})
public class Estoque implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7235115792354773677L;


	@Id	
	private int id;
	
	
	public Estoque(int id) {
		super();
		this.id = id;
	}

	public Estoque(int id, @Size(max = 5) List<Secao> secao) {
		super();
		this.id = id;
		this.secao = secao;
	}

	@Size(max=5)
	@OneToMany(mappedBy="estoque", cascade= CascadeType.ALL, fetch= FetchType.EAGER)
	@JsonManagedReference
	private List<Secao> secao;

	public Estoque() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Secao> getSecao() {
		return secao;
	}

	public void setSecao(List<Secao> listaSecao) {
		this.secao = listaSecao;
	}
	
	
}
