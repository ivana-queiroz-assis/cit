package br.com.ivana.cit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.data.util.ResponsavelEnum;
import br.com.ivana.cit.domain.Bebida;
import br.com.ivana.cit.response.Response;
import br.com.ivana.cit.service.BebidaService;

@RestController
@RequestMapping("/secao/")
public class BebidaController {

	@Autowired
	BebidaService bebidaService;
	
	@GetMapping("{idSecao}/bebidas")
	public ResponseEntity<Response<Bebida>> findAllBebidaBySecao(@PathVariable int idSecao) {
		
		Response<Bebida> response= bebidaService.findAllBebidaBy(idSecao);		
		if (response.temErrosNegocio()) {			
			return ResponseEntity.badRequest().body(response);
		}		
		return ResponseEntity.ok(response);
	}

	
	@PostMapping("{idSecao}/vendaBebida/{bebida}/{volume}/{responsavel}")
	public ResponseEntity<Response<Bebida>> retiraBebida(@PathVariable int idSecao,
			@PathVariable(value = "responsavel") ResponsavelEnum responsavel, @PathVariable int volumeDesejado,
			@PathVariable BebidaEnum typeBebida) {

		Response<Bebida> response = bebidaService.retiraVolumeBy(idSecao, volumeDesejado, responsavel, typeBebida);
		if (response.temErrosNegocio()) {			
			return ResponseEntity.badRequest().body(response);
		}		
		return ResponseEntity.ok(response);
				
	}

	@PostMapping("{idSecao}/addBebida/{responsavel}")
	public ResponseEntity<Response<Bebida>> adicionaBebida(@PathVariable int idSecao,
			@PathVariable(value = "responsavel") ResponsavelEnum responsavel, @RequestBody Bebida bebida,
			BindingResult result) {		
		Response<Bebida> response=new Response<Bebida>();
//		if (result.hasErrors()) {			
//			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
//		}
		response = bebidaService.salvaBebida(idSecao, responsavel, bebida);		
		if (response.temErrosNegocio()) {			
			return ResponseEntity.badRequest().body(response);
		}		
		return ResponseEntity.ok(response);		
			
	}
	@GetMapping("volumeTotalByType")
	public ResponseEntity<Response<String>> countAllVolumeByType() {
		Response<String> response = bebidaService.calculaVolumeTotalPorTipoBebida();
		return ResponseEntity.ok(response);
	}
	
}
