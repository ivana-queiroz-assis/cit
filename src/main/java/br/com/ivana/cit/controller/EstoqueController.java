package br.com.ivana.cit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.domain.Secao;
import br.com.ivana.cit.response.Response;
import br.com.ivana.cit.service.EstoqueService;

@RestController
@RequestMapping("/estoque/secao")
public class EstoqueController {

	@Autowired
	EstoqueService estoqueService;


	@GetMapping("getSecoesDisp/{volume}/{tipo}")
	public ResponseEntity<Response<Secao>> getSecoesDisponiveis(@PathVariable int volume, @PathVariable(value = "tipo") BebidaEnum tipo) {
		
		Response<Secao> response = new Response<Secao>();	
		if(!negativoOrZero(volume)) {
			response = estoqueService.getSecoesDisponiveisBy(volume,tipo);
		}else {
			response.getErrors().add("O volume não pode negativo ou zero");
		}
		return ResponseEntity.ok(response);
		
	}

	private boolean negativoOrZero(int number) {		
		return number>0? false: true;
	}


	
}
