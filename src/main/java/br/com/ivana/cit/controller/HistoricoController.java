package br.com.ivana.cit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.data.util.OrdenacaoEnum;
import br.com.ivana.cit.domain.Historico;
import br.com.ivana.cit.response.Response;
import br.com.ivana.cit.service.HistoricoService;

@RestController
@RequestMapping("/estoque/secao")
public class HistoricoController {

	@Autowired
	HistoricoService historicoService;

	@GetMapping("consultaTodoHistorico/{order}")
	public ResponseEntity<Response<Historico>> consultaHistorico(@PathVariable(value = "order") OrdenacaoEnum order) {
		
		Response<Historico> response = historicoService.getTodoHistorico(order);
		return ResponseEntity.ok(response);
	}

	@GetMapping("consultaHistoricoBySecaoAndBebida/{idSecao}/{tipo}")
	public ResponseEntity<Response<Historico>> consultaHistoricoBy(@PathVariable int idSecao,
			@PathVariable(value = "tipo") BebidaEnum tipo) {
		Response<Historico> response = historicoService.getHistoricoBy(idSecao, tipo);
		return ResponseEntity.ok(response);
	}

	@GetMapping("consultaHistoricoBySecao/{idSecao}")
	public ResponseEntity<Response<Historico>> consultaHistoricoBy(@PathVariable int idSecao) {
		Response<Historico> response = historicoService.getHistoricoBy(idSecao);
		return ResponseEntity.ok(response);
	}

	@GetMapping("consultaHistoricoByBebida")
	public ResponseEntity<Response<Historico>> consultaHistoricoBy(@PathVariable(value = "tipo") BebidaEnum tipo) {
		Response<Historico> response = historicoService.getHistoricoBy(tipo);
		return ResponseEntity.ok(response);
	}

	

}
