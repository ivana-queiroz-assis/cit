package br.com.ivana.cit.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import br.com.ivana.cit.domain.Bebida;

@Transactional
public interface BebidaRepository extends BebidaBaseRepository<Bebida> {

	List<Bebida> findBySecaoId(int secaoId);
	
}
