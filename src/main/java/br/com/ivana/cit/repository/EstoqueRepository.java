package br.com.ivana.cit.repository;

import java.util.List;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.domain.Estoque;
import br.com.ivana.cit.domain.Secao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



public interface EstoqueRepository extends JpaRepository<Estoque, Integer>{

	@Query("select s from Estoque e inner join e.secao s where s.volumeArmazenado + :qntBebida <= s.volumeTotal and s.bebidaType like :tipo ")
	List<Secao> getSecaoDisponivel(@Param("qntBebida") int qntBebida,@Param("tipo") BebidaEnum tipo);
	
	@Query("select s from Estoque e inner join e.secao s where s.bebidaType like :tipo")
	List<Secao> getSecaoByBebida(@Param("tipo") BebidaEnum tipo);


}
