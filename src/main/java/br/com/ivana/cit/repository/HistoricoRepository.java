package br.com.ivana.cit.repository;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.data.util.FluxoEnum;
import br.com.ivana.cit.domain.Historico;

public interface HistoricoRepository extends JpaRepository<Historico, Integer>{	
	
	List<Historico> findByBebidaTypeAndSecaoId(BebidaEnum bebidaType, int idSecao);
	
	List<Historico> findByBebidaType(BebidaEnum bebidaType);
	
	List<Historico> findBySecaoId(int secaoId);
	
	List<Historico> findByFluxo(FluxoEnum fluxo);
	
	List<Historico> findAllByOrderByDateTransacaoAsc();
	
	List<Historico> findAllByOrderBySecaoIdAsc();
	
	List<Historico> findAll(Sort sort);
	

	
}
