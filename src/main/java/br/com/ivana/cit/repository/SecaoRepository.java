package br.com.ivana.cit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.ivana.cit.domain.Secao;

public interface SecaoRepository extends JpaRepository<Secao, Integer> {

	@Query("select SUM(b.volume) from Secao s inner join s.bebidas b WHERE s.id = :idSecao")	 
	int countVolumeTotal(@Param("idSecao") int idSecao);
	
	
	
	
}
