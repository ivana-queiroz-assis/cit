package br.com.ivana.cit.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import br.com.ivana.cit.domain.BebidaNaoAlcoolica;

@Transactional
public interface BebidaNaoAlcoolicaRepository extends BebidaBaseRepository<BebidaNaoAlcoolica> {

	@Query("select SUM(u.volume) from #{#entityName} as u")	 
	Optional<Long> countVolumeTotal();
}
