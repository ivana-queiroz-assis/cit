package br.com.ivana.cit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import br.com.ivana.cit.domain.Bebida;

@NoRepositoryBean
public interface BebidaBaseRepository<T extends Bebida> extends JpaRepository<T, Integer>{

	
	
}
