package br.com.ivana.cit.data.loader;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.domain.Estoque;
import br.com.ivana.cit.domain.Secao;
import br.com.ivana.cit.repository.EstoqueRepository;
import br.com.ivana.cit.repository.SecaoRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {
	
	private EstoqueRepository repoEstoque;
	private SecaoRepository repoSecao;	
	
	@Autowired
	public DataLoader(EstoqueRepository repoEstoque, SecaoRepository repoSecao) {
		this.repoEstoque=repoEstoque;
		this.repoSecao= repoSecao;
	}
	
	@Override
	public void run(ApplicationArguments args)  {
		
		Estoque e= new Estoque(1);
		repoEstoque.save(e);
		List<Secao> listaSecao= new ArrayList<>(5);
		for(int i=1;i<=5;i++) {
			listaSecao.add(new Secao(i,"Seção "+i, e,BebidaEnum.SEM_TIPO, new Date()));
		}		
		repoSecao.saveAll(listaSecao);
		e.setSecao(listaSecao); 
		repoEstoque.save(e);		
		
	}
	
	
}
