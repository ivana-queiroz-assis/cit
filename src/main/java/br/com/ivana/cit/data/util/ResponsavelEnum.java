package br.com.ivana.cit.data.util;

public enum ResponsavelEnum {

	FUNCIONARIO,
	GERENTE,
	SUPERVISOR,
	COORDENADOR	
}
