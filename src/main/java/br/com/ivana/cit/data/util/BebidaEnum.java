package br.com.ivana.cit.data.util;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat
public enum BebidaEnum {
	ALCOOLICA,
	NAO_ALCOOLICA,
	SEM_TIPO	
	
}
