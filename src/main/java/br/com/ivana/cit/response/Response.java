package br.com.ivana.cit.response;

import java.util.ArrayList;
import java.util.List;

public class Response<T> {
	

	private T data;
	private List<String> errors= new ArrayList<String>();
	private List<T> listData = new ArrayList<T>();
	
	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<String> getErrors() {
		if(this.errors == null) {
			this.errors = new ArrayList<String>();
		}
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}

	public List<T> getListData() {
		return listData;
	}

	public void setListData(List<T> list) {
		this.listData = list;
	}

	public boolean temErrosNegocio() {
		return this.errors.isEmpty()? false: true;
	}
	
	

}
