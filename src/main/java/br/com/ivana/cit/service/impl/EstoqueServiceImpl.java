package br.com.ivana.cit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.domain.Secao;
import br.com.ivana.cit.repository.EstoqueRepository;
import br.com.ivana.cit.response.Response;
import br.com.ivana.cit.service.EstoqueService;

@Service
public class EstoqueServiceImpl implements EstoqueService {
	
	@Autowired
	EstoqueRepository repoEstoque;

	
	public Response<Secao> getSecoesDisponiveisBy(int volume, BebidaEnum tipo) {
		Response<Secao> response = new Response<Secao>();
		response.getListData().addAll(repoEstoque.getSecaoByBebida(BebidaEnum.SEM_TIPO));
		response.getListData().addAll(repoEstoque.getSecaoDisponivel(volume, tipo));
		return response;
	}

	

	
}
