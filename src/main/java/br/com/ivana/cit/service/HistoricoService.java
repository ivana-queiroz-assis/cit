package br.com.ivana.cit.service;

import org.springframework.stereotype.Component;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.data.util.FluxoEnum;
import br.com.ivana.cit.data.util.OrdenacaoEnum;
import br.com.ivana.cit.domain.Historico;
import br.com.ivana.cit.response.Response;

/**
 * Service responsável por todo o controle do histórico de entrada e saída. 
 * @author ivana
 *
 */
@Component
public interface HistoricoService {

	/**
	 * Função responsável por retornar todo o histórico incluindo entradas e saídas.
	 * 
	 * @return
	 */
	Response<Historico> getTodoHistorico(OrdenacaoEnum order);
	
	/**
	 * Função responsável por retornar o histórico por idSeção e tipo de Bebida
	 * 
	 * @param idSecao
	 * @param typeBebida
	 * @return
	 */
	public Response<Historico> getHistoricoBy(int idSecao, BebidaEnum typeBebida) ;
	
	
	/**
	 * Função responsável por retornar o histórico por idSeção 
	 * 
	 * @param idSecao
	 * @param typeBebida
	 * @return
	 */
	public Response<Historico> getHistoricoBy(int idSecao) ;
	
	/**
	 * Função responsável por retornar o histórico por tipo de bebida 
	 * 
	 * @param idSecao
	 * @param typeBebida
	 * @return
	 */
	public Response<Historico> getHistoricoBy(BebidaEnum typeBebida) ;
	
	/**
	 * Função responsável por retornar o histórico de acordo se house entrada ou saída
	 * 
	 * @param fluxoEnum
	 * @return
	 */
	public Response<Historico> getHistoricoBy(FluxoEnum fluxoEnum);
	
}
