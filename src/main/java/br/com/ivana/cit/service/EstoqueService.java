package br.com.ivana.cit.service;

import org.springframework.stereotype.Component;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.domain.Secao;
import br.com.ivana.cit.response.Response;


/**
 * Service responsável por todo o estoque. 
 * @author ivana
 *
 */
@Component
public interface EstoqueService {	

	/**
	 * Função responsável por retornar todas as seções disponíveis de acordo com o volume e o tipo. Inclui as seções SEM BEBIDA.
	 * 
	 * @param volume
	 * @param tipo
	 * @return
	 */
	Response<Secao> getSecoesDisponiveisBy(int volume, BebidaEnum tipo);

}
