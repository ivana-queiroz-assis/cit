package br.com.ivana.cit.service;

import org.springframework.stereotype.Component;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.data.util.ResponsavelEnum;
import br.com.ivana.cit.domain.Bebida;
import br.com.ivana.cit.response.Response;
/**
 * Service responsável por todo o controle de bebidas. 
 * @author ivana
 *
 */
@Component
public interface BebidaService {

	/**
	 * Função responsável por encontrar todas as bebidas por id da Seção.
	 * @param idSecao
	 * @return
	 */
	Response<Bebida> findAllBebidaBy(int idSecao);
	
	/**
	 * Função responsável por calcular o volume total por tipo de bebida
	 * @return
	 */
	Response<String> calculaVolumeTotalPorTipoBebida();
	
	/**
	 * Função responsável por retira volumes de bebidas da venda.
	 * 
	 * @param idSecao
	 * @param volumeDesejado
	 * @param responsavel
	 * @param typeBebida
	 * @return
	 */
	Response<Bebida> retiraVolumeBy(int idSecao, int volumeDesejado, ResponsavelEnum responsavel, BebidaEnum typeBebida);
	
	/**
	 * Função responsável por salvar bebida.
	 * 
	 * @param idSecao
	 * @param responsavel
	 * @param bebida
	 * @return
	 */
	Response<Bebida> salvaBebida(int idSecao, ResponsavelEnum responsavel, Bebida bebida);

}
