package br.com.ivana.cit.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.data.util.FluxoEnum;
import br.com.ivana.cit.data.util.ResponsavelEnum;
import br.com.ivana.cit.domain.Bebida;
import br.com.ivana.cit.domain.Historico;
import br.com.ivana.cit.domain.Secao;
import br.com.ivana.cit.repository.BebidaAlcoolicaRepository;
import br.com.ivana.cit.repository.BebidaNaoAlcoolicaRepository;
import br.com.ivana.cit.repository.BebidaRepository;
import br.com.ivana.cit.repository.EstoqueRepository;
import br.com.ivana.cit.repository.HistoricoRepository;
import br.com.ivana.cit.repository.SecaoRepository;
import br.com.ivana.cit.response.Response;
import br.com.ivana.cit.service.BebidaService;

@Service
public class BebidaServiceImpl implements BebidaService {

	@Autowired
	SecaoRepository repoSecao;

	@Autowired
	BebidaRepository repoBebida;

	@Autowired
	BebidaAlcoolicaRepository repoBebidaAlcoolica;

	@Autowired
	BebidaNaoAlcoolicaRepository repoBebidaNaoAlcoolica;

	@Autowired
	EstoqueRepository repoEstoque;

	@Autowired
	HistoricoRepository repoHistorico;

	public Response<Bebida> findAllBebidaBy(int idSecao) {

		Response<Bebida> response = new Response<Bebida>();
		if (!existeSecaoCom(idSecao)) {
			response.getErrors().add("A seção com ID: " + idSecao + " não existe.");
		} else {
			response.setListData(repoBebida.findBySecaoId(idSecao));
		}
		return response;
	}

	public Response<Bebida> retiraVolumeBy(int idSecao, int volumeDesejado, ResponsavelEnum responsavel,
			BebidaEnum typeBebida) {

		Response<Bebida> response = new Response<Bebida>();
		if (!existeSecaoCom(idSecao)) {
			response.getErrors().add("A seção com ID: " + idSecao + " não existe.");
		} else {
			List<Historico> historico = new ArrayList<Historico>();
			validaRegras(idSecao, volumeDesejado, typeBebida, response);
			if (response.temErrosNegocio()) {
				return response;
			}
			retiraBebidaParaVenda(idSecao, volumeDesejado, responsavel, response, historico);
			repoHistorico.saveAll(historico);
		}
		return response;
	}

	private void retiraBebidaParaVenda(int idSecao, int volumeDesejado, ResponsavelEnum responsavel,
			Response<Bebida> response, List<Historico> historico) {
		Secao s = repoSecao.findById(idSecao).get();
		List<Bebida> bebidas = repoBebida.findBySecaoId(idSecao);
		for (Bebida b : bebidas) {
			if (b.getVolume() < volumeDesejado) {
				Historico histSaida = new Historico(b.getVolume(), FluxoEnum.SAIDA, s, b, new Date(), responsavel);
				diminuiVolumeArmazenadoSecao(b.getVolume(), s);
				volumeDesejado -= b.getVolume();
				response.setData(b);
				historico.add(histSaida);
				b.diminuiVolume(b.getVolume());
				repoBebida.save(b);
			} else {
				Historico histSaida = new Historico(volumeDesejado, FluxoEnum.SAIDA, s, b, new Date(), responsavel);
				historico.add(histSaida);
				b.diminuiVolume(volumeDesejado);
				repoBebida.save(b);
				response.setData(b);
				s.diminuiVolumeArmazenado(volumeDesejado);
				break;
			}
		}
	}

	private void diminuiVolumeArmazenadoSecao(Integer volume, Secao s) {
		s.diminuiVolumeArmazenado(volume);
		if (s.getVolumeArmazenado() == 0) {
			s.alteraTipoBebidaAndVolume(BebidaEnum.SEM_TIPO);
			s.setUltimaAlteracaoTipo(new Date());
		}
	}

	private void validaRegras(int idSecao, int volumeDesejado, BebidaEnum typeBebida, Response<Bebida> response) {
		Secao s = repoSecao.findById(idSecao).get();
		if (secaoEstaSemBebida(s)) {
			response.getErrors().add("Essa seção ainda não tem bebidas adicionadas, id da Seção: " + idSecao);
		} else if (!existeSecaoCom(idSecao)) {
			response.getErrors().add("A seção com ID: " + idSecao + " não existe.");
		} else if (secaoJaTemTipoBebida(s, typeBebida)) {
			response.getErrors().add("A seção com ID  " + idSecao + " contém bebidas do tipo " + s.getBebidaType()
					+ " e você deseja bebidas do tipo " + typeBebida);
		} else if (isVolumeInsuficiente(s, volumeDesejado)) {
			response.getErrors()
					.add("A seção com ID  " + idSecao
							+ " não tem volume suficiente de bebidas para a venda. Volume armazenado: "
							+ s.getVolumeArmazenado());
		}
	}

	private boolean isVolumeInsuficiente(Secao s, int volumeDesejado) {
		return s.getVolumeArmazenado() < volumeDesejado ? true : false;
	}

	private boolean secaoJaTemTipoBebida(Secao s, BebidaEnum typeBebida) {
		return s.tipoBebidaIsTheSame(typeBebida);
	}

	private boolean secaoEstaSemBebida(Secao s) {

		if (s.getBebidaType().equals(BebidaEnum.SEM_TIPO)) {
			return true;
		} else {
			return false;
		}
	}

	private boolean existeSecaoCom(int id) {
		return repoSecao.findById(id).isPresent() ? true : false;
	}

	public Response<Bebida> salvaBebida(int idSecao, ResponsavelEnum responsavel, Bebida bebida) {

		Response<Bebida> response = new Response<Bebida>();
		if (!existeSecaoCom(idSecao)) {
			response.getErrors().add("A seção com ID: " + idSecao + " não existe.");

		} else {
			Secao s = repoSecao.findById(idSecao).get();

			if (secaoSemBebida(s)) {
				adicionaBebida(bebida, response, s);
				adicionaHistorico(bebida, FluxoEnum.ENTRADA, s, responsavel);

			} else if (secaoTemBebidaNaoAlcoolica(s)) {

				if (!estaAdicionandoBebidaMesmoTipoSecao(s, BebidaEnum.ALCOOLICA)) {
					response.getErrors().add("Não pode adicionar a Bebida Alcóolica com ID: " + bebida.getId()
							+ " em uma seção de Bebida Não Alcóolicas");
					;
				} else if (!temVolumeDisponivelSecao(s, bebida.getVolume())) {
					response.getErrors()
							.add("Não pode adicionar a bebida com ID " + bebida.getId()
									+ " nessa Seção, pois a ao adicionar a bebida não alcóolica o limite da sessão "
									+ s.getVolumeTotal() + " será ultrapassado");
				} else if (!s.podeTrocarTipo(bebida.getType())) {
					response.getErrors().add("Não pode trocar tipo de bebida da Seção, tente novamente amanhã. ");
				}
				{
					adicionaBebida(bebida, response, s);
					adicionaHistorico(bebida, FluxoEnum.ENTRADA, s, responsavel);
				}

			} else if (!temVolumeDisponivelSecao(s, bebida.getVolume())) {
				response.getErrors().add("Não pode adicionar a bebida com ID " + bebida.getId()
						+ " nessa Seção, pois a ao adicionar a bebida alcóolica o limite da sessão (500 ) será ultrapassado.");
			} else {
				adicionaBebida(bebida, response, s);
				adicionaHistorico(bebida, FluxoEnum.ENTRADA, s, responsavel);
			}
		}
		return response;
	}

	private boolean temVolumeDisponivelSecao(Secao s, Integer volume) {
		return repoSecao.countVolumeTotal(s.getId()) + volume <= s.getVolumeTotal();
	}

	private boolean estaAdicionandoBebidaMesmoTipoSecao(Secao s, BebidaEnum bebida) {
		return s.getBebidaType().equals(bebida) ? true : false;
	}

	private boolean secaoTemBebidaNaoAlcoolica(Secao s) {
		return s.getBebidaType().equals(BebidaEnum.NAO_ALCOOLICA) ? true : false;
	}

	private boolean secaoSemBebida(Secao s) {
		return s.estaSemBebida();
	}

	private void adicionaBebida(Bebida bebida, Response<Bebida> response, Secao s) {
		bebida.setSecao(s);

		s.alteraTipoBebidaAndVolume(bebida.getType());
		if (bebidaJaExiste(bebida.getId())) {
			atualizaVolumeArmazenadoCasoBebidaExista(bebida, s);
		} else {
			s.aumentaVolumeArmazenado(bebida.getVolume());
		}
		repoBebida.save(bebida);
		response.setData(bebida);

	}

	private void atualizaVolumeArmazenadoCasoBebidaExista(Bebida bebidaNova, Secao s) {
		Bebida bebidaBanco = repoBebida.findById(bebidaNova.getId()).get();
		s.diminuiVolumeArmazenado(bebidaBanco.getVolume());
		s.aumentaVolumeArmazenado(bebidaNova.getVolume());
	}

	private boolean bebidaJaExiste(int id) {
		return repoBebida.findById(id).isPresent() ? true : false;
	}

	private void adicionaHistorico(Bebida bebida, FluxoEnum fluxo, Secao secao, ResponsavelEnum responsavel) {
		Historico historico = new Historico(bebida.getVolume(), fluxo, secao, bebida, new Date(), responsavel);
		repoHistorico.save(historico);
	}

	public Response<String> calculaVolumeTotalPorTipoBebida() {

		Response<String> response = new Response<String>();
		Long alcool = repoBebidaAlcoolica.countVolumeTotal().isPresent() ? repoBebidaAlcoolica.countVolumeTotal().get()
				: 0;
		Long naoAlcool = repoBebidaNaoAlcoolica.countVolumeTotal().isPresent()
				? repoBebidaNaoAlcoolica.countVolumeTotal().get()
				: 0;
		String x = "{ alcoolica: " + alcool + ", nao alcoolica: " + naoAlcool;
		response.setData(x);
		return response;
	}
}
