package br.com.ivana.cit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.ivana.cit.data.util.BebidaEnum;
import br.com.ivana.cit.data.util.FluxoEnum;
import br.com.ivana.cit.data.util.OrdenacaoEnum;
import br.com.ivana.cit.domain.Historico;
import br.com.ivana.cit.repository.HistoricoRepository;
import br.com.ivana.cit.response.Response;
import br.com.ivana.cit.service.HistoricoService;
@Service
public class HistoricoServiceImpl implements HistoricoService{
	
	@Autowired
	HistoricoRepository repoHistorico;

	
	public Response<Historico> getTodoHistorico(OrdenacaoEnum order) {
		Response<Historico> response = new Response<Historico>();
		
		if(order.equals(OrdenacaoEnum.DATE)) {
			response.setListData(repoHistorico.findAllByOrderByDateTransacaoAsc());
		}else if(order.equals(OrdenacaoEnum.SECAO)) {
			response.setListData(repoHistorico.findAllByOrderBySecaoIdAsc());
		}else {
			response.setListData(repoHistorico.findAll(orderBySecaoIdAscAndDateAsc()));
		}		
		return response;
	}
	
	private Sort orderBySecaoIdAscAndDateAsc() {
	    return new Sort(Sort.Direction.ASC, "secaoId")
	                .and(new Sort(Sort.Direction.ASC, "dateTransacao"));
	}
	
	public Response<Historico> getHistoricoBy(int idSecao, BebidaEnum typeBebida) {
		Response<Historico> response = new Response<Historico>();
		response.setListData(repoHistorico.findByBebidaTypeAndSecaoId(typeBebida, idSecao));		
		return response;
	}
	
	public Response<Historico> getHistoricoBy(int idSecao) {
		Response<Historico> response = new Response<Historico>();
		response.setListData(repoHistorico.findBySecaoId(idSecao));
		return response;
	}
	
	public Response<Historico> getHistoricoBy(BebidaEnum typeBebida) {
		Response<Historico> response = new Response<Historico>();
		response.setListData(repoHistorico.findByBebidaType(typeBebida));
		return response;
	}
	
	public Response<Historico> getHistoricoBy(FluxoEnum fluxoEnum) {
		Response<Historico> response = new Response<Historico>();
		response.setListData(repoHistorico.findByFluxo(fluxoEnum));
		return response;
	}
	
	
}
